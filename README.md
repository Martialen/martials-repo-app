# SCF <> Martial Jefferson

This is a single page application that exposes event data for user-specified, public github repositories.

To use the app, you'll need to have NodeJS (preferably >= 8.2x) installed on your local machine.

## Setup

Clone the repo to a local directory, then run:

```
npm install
````

This will install the server app and relevant test modules.

Setup the client-side PolymerJS app with:

```
cd client
bower install
```

Then return to the project directory and run `npm run serve` to start the app.

## Documentation

To generate project documentation run `npm run generate-docs` from the main directory.

Open `index.html` in the generated `docs` directory to view the documentation.

## Testing

Testing is powered by AVAJS. To view the tests run `npm test` in the project directory.