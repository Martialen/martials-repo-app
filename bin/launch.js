const App = require('../server/app');
const http = require('http');

let app = new App();
let server = http.createServer(app);

server.on('listening', e => {
  console.log(`Server started at http://localhost:${server.address().port}`);
});

server.on('error', console.error);

server.listen(0);