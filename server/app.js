// Module Dependencies
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');

/**
 * Allows users to query Github for repository event data.
 * 
 * @class RepoApp
 */
class RepoApp {
  /**
   * Creates an instance of RepoApp.
   * @memberof RepoApp
   */
  constructor(){
    let app = express();
    app.use(bodyParser.json());
    app.get('/api/v1/events', this.validateIncomingRequest)
    app.get('/api/v1/events', this.fetchEventData);
    app.use('/', express.static('client'));
    app.use(this.notFound);
    return app;
  }

  /**
   * Queries Github API and returns relevant repository events.
   * 
   * @param {any} req 
   * @param {any} res 
   * @param {any} next 
   * @memberof RepoApp
   */
  async fetchEventData(req, res, next){
    let { owner, repo } = req.query;
    
    let apiRequest = `https://api.github.com/repos/${owner}/${repo}/events`;

    let { data : events } = await axios.get(apiRequest).catch( e => {
      // present GH error responses as an empty object
      return {};
    });

    res.json( events || [] );
  }

  /**
   * Handles requests to unknown routes.
   * 
   * @param {any} req 
   * @param {any} res 
   * @memberof RepoApp
   */
  notFound(req, res){
    res.status(404).send("This page does not exist.");
  }

  /**
   * Handles malformed requests.
   * 
   * @param {any} req 
   * @param {any} res 
   * @memberof RepoApp
   */
  validateIncomingRequest(req, res, next){
    let { owner, repo } = req.query;

    if( owner === undefined || repo === undefined ){
      res.status(400).send(`Bad request. Please specify both owner and repo.`);
    } else {
      next();
    }
  }
}
// (new RepoApp).listen(4040);
module.exports = RepoApp;