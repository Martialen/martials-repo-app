import test from 'ava';
import request from 'supertest';
import RepoApp from './server/app';

// Create a fresh instance of the app for each test:
test.beforeEach('create an instance of the Repo app', t => {
  t.context.app = new RepoApp;
  t.context.eventsEndpoint = '/api/v1/events';
  t.context.validParameters = { owner: 'nodejs', repo: 'node'};
  t.context.invalidParameters = {};
});

// Define basic test cases
test('repo:server should provide an endpoint to retrieve events', async (t) => {
  let { app, eventsEndpoint, validParameters } = t.context;

  let response = await request(app).get(eventsEndpoint).query(validParameters);
  t.is(response.status, 200);
});

test('repo:server should handle requests to unknown routes', async (t) => {
  let { app } = t.context;

  let response = await request(app).get('/api/v1/theDisappointmentsRoom');
  t.is(response.status, 404);
});

test('repo:server should issue appropriate response to incomplete requests', async (t) => {
  let { app, invalidParameters } = t.context;
  let response = await request(app).get('/api/v1/events').query(invalidParameters);
  t.is(response.status, 400);
});

test('repo:server should answer valid requests with JSON data', async (t) => {
  let { app, eventsEndpoint, validParameters } = t.context;

  let response = await request(app).get(eventsEndpoint).query(validParameters).expect('Content-Type', /^application\/json/);
  t.pass();
});

test('repo:server should serve client app on its main route', async (t) => {
  let { app } = t.context;

  let response = await request(app).get('/').expect('Content-Type', /^text\/html/);
  t.pass();
});